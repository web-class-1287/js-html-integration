// Что бы взаимодействовать с элементами html, их нужно найти

// обращаясь к document.querySelector() мы ищем по id, которые указали в html
// И при сваеваем каждой переменной свой элемент
let counter = document.querySelector('#counter')
let incrementer = document.querySelector('#incrementer')

// Создаем новую и называем ее "increaseCounter"
function increaseCounter() {
    // counter - это наш span в html, 
    // а counter.innerHTML - это содержание ( текст ) этого span

    // В это строке мы изменяем содержание span на такое же как в нем сейчас, но больше на единицу
    counter.innerHTML = +counter.innerHTML + 1

    // Так как counter.innerHTML - это строка и поумолчанию там находится '0'
    // И если мы сложим строку '0' и 1, JS приведет 1 к строке и сложит их вот таким образом '01'
    // Что бы этого не происходило мы ставим + перед строкой, говоря JS, что бы он превратил
    // counter.innerHTML в число, а после сложил 2 числа и получившееся значение присвоил нашему counter.innerHTML
}

// Здесь мы говорим, что по клику на элемент incrementer, будет выполняться функция,
// которую мы записали выше
incrementer.onclick = increaseCounter